var execSync = require('child_process').execSync;
module.exports = {
  publicPath: process.env.GITLAB_CI ? '/runner-tracker/' : '/',
  /**
   * @param config {import('webpack').Configuration}
  */
  chainWebpack: config => {
    config.devtool(process.env.NODE_ENV == "production" ? 'source-map' : 'source-map');
    config.mode(process.env.NODE_ENV);
    config.module
      .rule('wasm')
      .test(/\.wasm$/)
      .type("javascript/auto")
      .use('file-loader')
        .loader('file-loader')
        .end()
    ;
    config.plugin('define')
      .tap(args => {
        var version = execSync('git describe').toString().trim().replace(/-(.*?)-.*$/, '.$1');
        args[0] = {
          ...(args[0] || {}),
          'process.env.VERSION': JSON.stringify(version),
          'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
          '__USE_MONGODB__': process.env.db === 'mongodb',
          '__USE_INDEXEDDB__': process.env.db === 'indexeddb',
          'isDev': process.env.NODE_ENV === 'development',
        };
        return args;
      })
    ;
  },
  pwa: {
    themeColor: '#00BCD6',
    msTileColor: '#00BCD6',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black-translucent',
    manifestOptions: {
      name: 'Runner Tracker',
      short_name: 'Runner Tracker',
      start_url: '/',
    },
    workboxOptions: {
      navigateFallback: '/index.html',
    },
  },
  transpileDependencies: ['vuetify','@hamstudy/flamp']
};
