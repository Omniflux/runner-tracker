import 'material-icons/iconfont/material-icons.css';

import Vue from 'vue';
import { } from 'vuetify'; // This is so typescript can find 'vuetify/lib'
import Vuetify from 'vuetify/lib';
import scssVariables from '@/variables.scss';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    // iconfont: 'mdi',
    iconfont: 'md',
  },
  theme: {
    themes: {
      light: {
        hamstudyBlue: {
          base: scssVariables.hamstudyBlue,
          darken1: scssVariables.primary,
        },
        hamstudyYellow: {
          base: scssVariables.accent,
        },
        primary: scssVariables.primary,
        secondary: scssVariables.secondary,
        accent: scssVariables.accent,
        error: scssVariables.error,
        info: scssVariables.info,
        success: scssVariables.success,
        warning: scssVariables.warning,
      },
      dark: {
        hamstudyBlue: {
          base: scssVariables.hamstudyBlue,
          darken1: scssVariables.primary,
        },
        hamstudyYellow: {
          base: scssVariables.accent,
        },
        primary: scssVariables.primary,
        secondary: scssVariables.secondary,
        accent: scssVariables.accent,
        error: scssVariables.error,
        info: scssVariables.info,
        success: scssVariables.success,
        warning: scssVariables.warning,
      },
    },
  },
});
