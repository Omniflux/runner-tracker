import Vue from 'vue';
import { formatDate } from '@/lib/formatDate';

export function normalizeTimeStr(value: number | string | Date | null): string {
  if (typeof value === 'string' || typeof value === 'number') {
    value = String(value || '');
    value.replace(':', '');
    while (value.length < 4) {
      value = '0' + value;
    }
    value = value.substr(value.length - 4);
    // At this point we should have a string with four numbers eg: "0000"
    value = value.substr(0, 2) + ':' + value.substr(2, 2);
    return value;
  } else if (value === null || value === (void 0)) {
    return '';
  } else {
    return value.toTimeString ? value.toTimeString().substr(0, 5) : '';
  }
}
Vue.filter('time', (value: number | Date | string) => {
  return normalizeTimeStr(value);
});

Vue.filter('date', (date: Date, format?: string) => {
  return formatDate(date, format || 'MM/DD HH:mm');
});

Vue.filter('day', (date: Date) => {
  return formatDate(date, 'DD');
});
