appVersion: "App version: {{0}}"

file:
  blockData: Block Data

filter:
  all: All
  expecting: Expecting
  inStation: In Station
  needsTransmission: Not Transmitted
  seen: Recent

grid:
  eta: "ETA: {{time}}"

inputTimes:
  runnerNumber: "Runner Number"
  timeIn: "In:{{0}}"
  timeOut: "Out:{{0}}"
  instructions:
    runnerNumber: "Enter the runner's bib number"
    timeIn: "Time In"
    timeOut: "Time Out"
    found: "Runner #{{bibNumber}}"
    notFound: "Runner {{0}} not found."
    add: "To add this runner tap 'Add'"

receive:
  delete: Delete
  historyDetails: "Received {{0}} ({{1}})"
  historyDetailsIncoming: "Receiving {{0}}"
  import: Import
  importing: Importing data
  importNow: Import Now
  imported: Data imported
  importUnknown: "Trying to import data"
  incoming: Receiving a transmission with hash {{hash}}
  runnersImported: "{{0}} runner records received"
  view: View

runner:
  age: "Age: {{0}}"
  gender: "Gender: {{0}}"
  home: "Home: {{0}}"
  firstName: First
  lastName: Last
  name: "Name: {{0}} {{1}}"
  note: "Note: {{0}}"
  team: "Team: {{0}}"
  dns: DNS
  didNotStart: Did not start
  dropped: DNF
  droppedAt: "DNF at station {{0}}"
  droppedReason: "DNF because {{0}}"
  dnsReason: "DNS because {{0}}"

  bibNumber: Bib Number
  notAvailable: "—"
  runner: Runner

  delete: Delete this participant?
  deleteEntry: Delete participant's time entry?
  deleteEntry_plural: Delete these participants' time entries?

runnerCard:
  bibNumber: "Runner #{{0}}"
  timeIn: "In:{{0}}"
  timeOut: "Out:{{0}}"
  note: "Note: {{0}}"
  notRecorded: Not Recorded

runnerDetails:
  title: "Runner #{{0}}"
  noEntrys: No entrys found
  timeIn : "Time in:{{0}}"
  timeOut: "Time out:{{0}}"

runnersList:
  allRunners: All Runners
  noData: There is no data for any runners
  noRunnersInStation: No runners in this station
  runnersInCurrentStation: Runners in station {{0}}

settings:
  aidStation: Aid Station
  alwaysListening: Always Listening
  darkMode: Dark mode
  event: Event
  inputTimesSection: Time Input
  sendReceiveSection: Send / Receive
  myCallsign: What is the operator's callsign
  pickEvent: Switch event
  skipTimeIn: Time out only input
  skipTimeOut: Time in only input
  station: Station
  useDialpadArrangement: Dialpad view
  # noEvents: Press the mic to listen for event data or the plus to manually add a new event
  noEvents: Press the plus to add a new race event
  noEventsFile: or if you have a file with the race data press the attach file button
  noStations: To add a station press the plus
  deleteEvent: Delete event {{name}}?
  editRaceTitle: Edit Race Event
  editStationTitle: Edit Station

station:
  name_short: "(AS {{0}})"
  distance: "Station distance: {{0}}"
  dnfDialogTitle: Mark runner as DNF
  dnsDialogTitle: Mark runner as DNS
  dnfReason: DNF because...
  dnsReason: DNS because...

stationEntryCard:
  station: Station {{0}}

stationPicker:
  add: Add station

tabs:
  grid: Grid
  inputTimes: Input
  runnersList: Runners
  settings: Settings
  entries: Records
  receive: Receive
  transmit: Send/Receive
  raceEventPicker: Event

titles:
  runners: $t(tabs.runnersList)
  settings: $t(tabs.settings)
  station: "Station {{0}}"
  transmit: $t(tabs.transmit)
  stationPicker: Station
  eventPicker: Event
  viewTransmission: Transmission contents

transmit:
  blocksToTransmit: Select which blocks to re-transmit
  customMessage: Message to transmit
  dataNeedingToTransmit: Runner data needing to be transmitted
  hash: "Hash: {{0}}"
  headers: Headers
  history: History
  historyDetails: "Sent {{0}} record"
  historyDetails_plural: "Sent {{0}} records"
  sentMessage: "Sent {{0}} ({{1}})"
  listening: Listening...
  missingBlocks: The red ones are missing blocks (ask for these to be sent again)
  noData: "No runner data needs to be transmitted"
  noTransmissions: No history
  receivedTab: Received
  transmittedTab: Transmitted
  deleteTransmission: Delete transmission?

unsupported: Sorry, this app will not work on this device :(
errorLoading: Oh no! The app couldn't load!

months:
  0: January
  0_short: Jan
  1: February
  1_short: Feb
  2: March
  2_short: Mar
  3: April
  3_short: Apr
  4: May
  4_short: May
  5: June
  5_short: Jun
  6: July
  6_short: Jul
  7: August
  7_short: Aug
  8: September
  8_short: Sep
  9: October
  9_short: Oct
  10: November
  10_short: Nov
  11: December
  11_short: Dec

days:
  0: Sunday
  0_short: Sun
  1: Monday
  1_short: Mon
  2: Tuesday
  2_short: Tue
  3: Wednesday
  3_short: Wed
  4: Thursday
  4_short: Thu
  5: Friday
  5_short: Fri
  6: Saturday
  6_short: Sat