export interface SNACKBAR {
  i18next?: string;
  message?: string;
  timeout?: number;

  bottom?: boolean;
  left?: boolean;
  right?: boolean;
  top?: boolean;
}
