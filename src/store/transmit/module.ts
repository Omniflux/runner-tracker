import { Amp } from '@hamstudy/flamp';
import pathify, { make } from 'vuex-pathify';
import { ActionInterface, RTActionContext } from '@/store';
import { Transmission } from '@/models/transmission';
import { SNACKBAR } from './types';


const PATHIFY_MAPPING = pathify.options.mapping;
pathify.options.mapping = 'simple';

const moduleState = {
  history: [] as Transmission[],
  snackbar: null as SNACKBAR | null,
  transmission: null as Transmission | null,
};
export type TransmitState = typeof moduleState;
type ActionContext = RTActionContext<TransmitState>;

// GETTERS
const moduleGetters = {};

// MUTATIONS
const moduleMutations = {
  ...make.mutations(moduleState),

  setTransmission(state: TransmitState, payload: Transmission | null) {
    let transmission = state.transmission;
    if (transmission && transmission.transmitting) { transmission.stop(); }
    state.transmission = payload;
  },

  stopTransmission(state: TransmitState) {
    let transmission = state.transmission;
    if (transmission && transmission.transmitting) { transmission.stop(); }
    state.transmission = null;
  },

  startTransmission(state: TransmitState) {
    let transmission = state.transmission;
    if (transmission && transmission.transmitting) { transmission.stop(); }
    if (transmission) {
      transmission.transmit();
    }
  },
};

// ACTIONS
const moduleActions = {
  ...make.actions(moduleState),

  async updateHistoryList({dispatch, rootState}: ActionContext) {
    const eventId = rootState.settings.raceEventId;
    const txTransmissions = await Transmission.getTxDocs(eventId);
    const rxTransmissions = await Transmission.getRxDocs(eventId);
    const history = [...txTransmissions, ...rxTransmissions];
    history.sort((x,y) => x.timestamp > y.timestamp ? -1 : 1);
    dispatch('setHistory', history);
  },

  async getTransmission({rootState}: ActionContext, hash: string) {
    const eventId = rootState.settings.raceEventId;
    const [tx, rx] = await Promise.all([
      Transmission.getTxDoc(hash, eventId),
      Transmission.getRxDoc(hash, eventId),
    ]);
    return tx || rx;
  },

  async deleteTransmission({dispatch}: ActionContext, transmission: Transmission) {
    await Transmission.delete(transmission);
    await dispatch('updateHistoryList');
  },

  async saveHistoryList({rootState}: ActionContext, payload: { amp: Amp; txOrRx: 'tx' | 'rx'; }) {
    const raceEventId = rootState.settings.raceEventId;
    const transmission = Transmission.fromAmp(raceEventId, payload.amp, payload.txOrRx);
    await transmission.save();
  },
};
export type Actions = ActionInterface<typeof moduleActions, TransmitState>;

export default {
  namespaced: true,
  state: moduleState,
  mutations: moduleMutations,
  actions: moduleActions,
  getters: moduleGetters,
};


pathify.options.mapping = PATHIFY_MAPPING;
