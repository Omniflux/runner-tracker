type GenericFunction = (...args: any[]) => any;
interface GenericObject { [key: string]: any; }

type OmitFirstArg<F extends GenericFunction> = F extends (x: any, ...args: infer P) => infer R ? (...args: P) => R : never;

type RenamePropToSetAction<T extends string> = `set${Capitalize<T>}`;

/**
 * Used to conver a vuex module's action functions type to an actions interface
 * Thus you can then get the type of the function with correct payload by exporting
 * this type.
 *
 * Assuming you're using make.actions from vuex-pathify you can pass in the moduleState
 * to add types for all the actions vuex-pathify makes via make.actions
 *
 * In vuex module.ts
 * export type Actions = ActionInterface<moduleActions, moduleState>;
 *
 * In the vue component file import Actions
 * actionFunction!: Actions['doThing'];
 */
export type ActionInterface<MA extends {}, S = {}> = {
  [P in keyof MA]: MA[P] extends GenericFunction ? OmitFirstArg<MA[P]> : MA[P];
} & {
  [P in keyof S as P extends string ? RenamePropToSetAction<P> : P]: (payload: S[P]) => Promise<void>;
};

export type AllActions<Mods extends GenericObject> = {
  [Mod in keyof Mods]: ActionInterface<Mods[Mod]['actions'], Mods[Mod]['state']>;
};

export type GetModuleStates<T extends GenericObject> = {
  [P in keyof T]: T[P]['state'];
};
