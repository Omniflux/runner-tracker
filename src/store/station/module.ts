import pathify, { make } from 'vuex-pathify';
import { Station } from '@/models/station';
import { ActionInterface, RTActionContext } from '@/store';

const PATHIFY_MAPPING = pathify.options.mapping;
pathify.options.mapping = 'simple';

const moduleState = {
  station: null as any as Station,
};
export type StationState = typeof moduleState;
type ActionContext = RTActionContext<StationState>;

// GETTERS
const moduleGetters = {};

// MUTATIONS
const moduleMutations = {
  ...make.mutations(moduleState),
};

// ACTIONS
const moduleActions = {
  ...make.actions(moduleState),
};
export type Actions = ActionInterface<typeof moduleActions, StationState>;

export default {
  namespaced: true,
  state: moduleState,
  mutations: moduleMutations,
  actions: moduleActions,
  getters: moduleGetters,
};

pathify.options.mapping = PATHIFY_MAPPING;
