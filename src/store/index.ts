import Vue from 'vue';
import Vuex, { ActionContext, Store } from 'vuex';
import { VuexPersistence } from 'vuex-persist';
import { GetModuleStates } from './types';

export type { ActionInterface } from './types';

import { listenForDarkThemeChange } from './settings/module';
import modules from './loader';

// import { Plugins } from '@capacitor/core';
// const { Storage } = Plugins;

interface Modules {
  inputTimes: typeof import('./inputTimes/module').default;
  participants: typeof import('./participants/module').default;
  station: typeof import('./station/module').default;
  settings: typeof import('./settings/module').default;
  transmit: typeof import('./transmit/module').default;
}

const APP_VERSION = process.env.VERSION;
const state = {
  version: APP_VERSION,
};
type RootStateOnly = typeof state;
export interface RootState extends GetModuleStates<Modules>, RootStateOnly {}
export type RTActionContext<T> = ActionContext<T, RootState>;

Vue.use(Vuex);

const persistSettings = new VuexPersistence<any>({
  key: 'Settings',
  reducer: (s) => ({ settings: s.settings }),
  storage: window.localStorage,
  // storage: Storage,
});
// function vuexActionsPlugin(s: Store<RootState>) {
//   Object.defineProperty(s, 'actions', {
//     get() {
//       return Object.keys((s as any)._actions).reduce((obj, type) => {
//         const [ns, action] = type.split('/');
//         if (!obj[ns]) { obj[ns] = {}; }
//         obj[ns][action] = (payload: any) => s.dispatch.call(type, payload);
//         return obj;
//       }, {} as {[key: string]: {[key: string]: (payload: any) => Promise<void>}});
//     },
//   });
// }

const store = new Vuex.Store<RootState>({
  state: state as any,
  modules,
  plugins: [persistSettings.plugin],
  strict: process.env.NODE_ENV !== 'production',
});
// Maybe get around to using this AllActions plugin if deemed trustworthy
// }) as Store<RootState> & { actions: AllActions<Modules>};

listenForDarkThemeChange(store);

export default store;

if (isDev) {
  (window as any).store = store;
}
