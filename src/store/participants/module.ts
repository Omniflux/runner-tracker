import pathify, { make } from 'vuex-pathify';
import { ActionInterface, RTActionContext } from '@/store';

import { Participant } from '@/models/participant';
import { Entry } from '@/models/entry';

const PATHIFY_MAPPING = pathify.options.mapping;
pathify.options.mapping = 'simple';

const moduleState = {
  focusedEntry: null as Entry | null,
  focusedParticipant: null as Participant | null,
  bibNumber: null as number | null,
};
export type ParticipantState = typeof moduleState;
type ActionContext = RTActionContext<ParticipantState>;

// GETTERS
const moduleGetters = {};

// MUTATIONS
const moduleMutations = {
  ...make.mutations(moduleState),
  setTimeIn(state: ParticipantState, {entry, timeIn}: {entry: Entry, timeIn: Date | null}) {
    entry.timeIn = timeIn;
  },
  setTimeOut(state: ParticipantState, {entry, timeOut}: {entry: Entry, timeOut: Date | null}) {
    entry.timeOut = timeOut;
  },
};

// ACTIONS
const moduleActions = {
  ...make.actions(moduleState),

  async findAndUpdateParticipant({dispatch, rootState}: ActionContext, bibNumber: number | null) {
    const raceId = rootState.settings.raceEventId;
    const stationId = rootState.settings.station;
    dispatch('setBibNumber', bibNumber);
    let participant = bibNumber === null ? null : await Participant.getParticipant(raceId, bibNumber);
    if (!participant) {
      dispatch('setFocusedParticipant', null);
      dispatch('setFocusedEntry', null);
      return;
    }
    let entry = await Entry.getEntry(stationId, participant.id);
    if (!entry) { entry = new Entry(raceId, participant.id, stationId); }
    dispatch('setFocusedParticipant', participant);
    dispatch('setFocusedEntry', entry);
  },

  async addParticipant({dispatch, rootState}: ActionContext, bibNumber: number) {
    const raceId = rootState.settings.raceEventId;
    let participant = new Participant(raceId, { bibNumber });
    dispatch('setFocusedParticipant', participant);
    await participant.save();
  },

  async saveTimeToParticipant({commit, dispatch, rootState}: ActionContext, {participant, entry, timeIn, timeOut}: { participant: Participant; entry: Entry | null; timeIn?: Date | null; timeOut?: Date | null; }) {
    const raceId = rootState.settings.raceEventId;
    const stationId = rootState.settings.station;
    if (!entry) { entry = new Entry(raceId, participant.id, stationId); }
    if (timeIn || timeIn === null) {
      commit('setTimeIn', {entry, timeIn});
    }
    if (timeOut || timeOut === null) {
      commit('setTimeOut', {entry, timeOut});
    }
    dispatch('setFocusedEntry', entry);
    await entry.save();
  },
};
export type Actions = ActionInterface<typeof moduleActions, ParticipantState>;

export default {
  namespaced: true,
  state: moduleState,
  mutations: moduleMutations,
  actions: moduleActions,
  getters: moduleGetters,
};


pathify.options.mapping = PATHIFY_MAPPING;
