import pathify, { make } from 'vuex-pathify';
import { ActionInterface, RTActionContext } from '@/store';
import { normalizeTimeStr } from '@/plugins/timeFilter';

const PATHIFY_MAPPING = pathify.options.mapping;
pathify.options.mapping = 'simple';

const moduleState = {
  focus: 'runnerNumber' as "runnerNumber" | "timeIn" | "timeOut",
  timeIn: null as { number: number; isValid: boolean } | null,
  timeInDay: '',
  timeOut: null as { number: number; isValid: boolean } | null,
  timeOutDay: '',
};
export type InputState = typeof moduleState;
type ActionContext = RTActionContext<InputState>;

// GETTERS
const moduleGetters = {};

// MUTATIONS
const moduleMutations = {
  ...make.mutations(moduleState),
};

// ACTIONS
const moduleActions = {
  ...make.actions(moduleState),

  setTimeIn({commit, state}: ActionContext, payload: string | number | Date | null) {
    commit('timeIn', getTime(payload));
    if (payload === null) {
      commit('timeInDay', '');
    } else if (payload instanceof Date && !isNaN(new Date(payload).getTime())) {
      commit('timeInDay', new Date(payload).toLocaleDateString());
    }
    if (state.timeIn?.isValid && state.timeOut?.isValid && !state.timeInDay) {
      const today = new Date().toLocaleDateString();
      const timeInDate = new Date(`${today} ${state.timeIn?.number ?? null}`);
      if (timeInDate > new Date(`${today} ${state.timeOut?.number ?? null}`)) {
        timeInDate.setDate((timeInDate.getDate() - 1));
        commit('timeInDay', timeInDate.toLocaleDateString());
      }
    }
  },

  setTimeOut({commit, state}: ActionContext, payload: string | number | Date | null) {
    commit('timeOut', getTime(payload));
    if (payload === null) {
      commit('timeOutDay', '');
    } else if (payload instanceof Date && !isNaN(new Date(payload).getTime())) {
      commit('timeOutDay', new Date(payload).toLocaleDateString());
    }
    if (state.timeOut?.isValid && state.timeIn?.isValid && !state.timeOutDay) {
      const today = new Date().toLocaleDateString();
      const timeOutDate = new Date(`${today} ${normalizeTimeStr(state.timeOut?.number ?? null)}`);
      if (timeOutDate > new Date(`${today} ${normalizeTimeStr(state.timeOut?.number ?? null)}`)) {
        timeOutDate.setDate((timeOutDate.getDate() - 1));
        commit('timeOutDay', timeOutDate.toLocaleDateString());
      }
    }
  },

  setTimeInDay({commit}: ActionContext, payload: string | Date) {
    if (typeof payload === 'string') {
      let match = /(\d\d\d\d)-(\d\d)-(\d\d)/.exec(payload);
      if (!match) { return; }
      let [, year, month, day] = match;
      payload = new Date(Number(year), Number(month)-1, Number(day));
    }
    if (!isNaN(payload.getTime())) {
      commit('timeInDay', new Date(payload).toLocaleDateString());
    }
  },

  setTimeOutDay({commit}: ActionContext, payload: string | Date) {
    if (typeof payload === 'string') {
      let match = /(\d\d\d\d)-(\d\d)-(\d\d)/.exec(payload);
      if (!match) { return; }
      let [, year, month, day] = match;
      payload = new Date(Number(year), Number(month)-1, Number(day));
    }
    if (!isNaN(payload.getTime())) {
      commit('timeOutDay', new Date(payload).toLocaleDateString());
    }
  },
};
export type Actions = ActionInterface<typeof moduleActions, InputState>;

export default {
  namespaced: true,
  state: moduleState,
  mutations: moduleMutations,
  actions: moduleActions,
  getters: moduleGetters,
};


pathify.options.mapping = PATHIFY_MAPPING;


function getTime(time: string | number | Date | null) {
  if (time instanceof Date && isNaN(time.getTime())) { time = null; }
  if (time === null) {
    return null;
  } else if (typeof time === 'number' || typeof time === 'string') {
    const today = new Date().toLocaleDateString();
    return {
      number: Number(String(time).substr(-10)),
      isValid: !isNaN(new Date(`${today} ${normalizeTimeStr(time)}`).getTime()),
    };
  } else {
    return {
      number: Number(time.toTimeString().substr(0, 5).replace(':', '')),
      isValid: true,
    };
  }
}
