// Register each file as a corresponding Vuex module. Module nesting
// will mirror [sub-]directory hierarchy and modules are namespaced
// as the camelCase equivalent of their file name.

interface IModules {
  modules: IModule;
}

interface IModule {
  [name: string]: IModules;
}

import camelCase from 'lodash/camelCase';

// https://webpack.js.org/guides/dependency-management/#require-context
const requireModule = (require as any).context(
  // Search for files in the current directory
  '.',
  // Search for files in subdirectories
  true,
  // Include any .ts files that are not unit tests
  /\bmodule.ts$/,
);
const root: IModules = { modules: {} };

// Recursively get the namespace of the module, even if nested
function getNamespace(subtree: IModules, path: string[]): IModules {
  if (path.length === 1) { return subtree; }

  const namespace = path.shift() as string;
  // Set a placeholder modules obj but overwrite with any previous modules
  subtree.modules[namespace] = { ...{modules: {}}, ...subtree.modules[namespace] };
  return getNamespace(subtree.modules[namespace], path);
}

requireModule.keys().forEach((fileName: string) => {
  // Skip this file, as it's not a module
  if (fileName === './loader.ts') { return; }

  // Get the module path as an array
  const modulePath = fileName
    // Remove the "./" from the beginning
    .replace(/^\.\//, '')
    // Remove the file extension from the end
    .replace(/\/module\.\w+$/, '')
    // Split nested modules into an array path
    .split(/\//)
    // camelCase all module namespaces and names
    .map(camelCase);

  // Get the modules object for the current path
  const { modules } = getNamespace(root, modulePath);

  // Add the module to our modules object
  modules[modulePath.pop() as string] = {
    ...(requireModule(fileName).default),
    // Modules are namespaced by default
    namespaced: true,
  };
});

export default root.modules;
