import pathify, { make } from 'vuex-pathify';
import { Event } from '@/models/event';
import { Station } from '@/models/station';
import store, { ActionInterface, RTActionContext, RootState } from '@/store';
import { FILTER_RUNNERS_TO, NUMPAD } from './types';

const PATHIFY_MAPPING = pathify.options.mapping;
pathify.options.mapping = 'simple';

const darkMQL = window.matchMedia?.("(prefers-color-scheme: dark)");

const moduleState = {
  authentication: '',
  callsign: '',
  DARK_MODE: getDarkStatus(),
  serverUri: '',
  useDialpadArrangement: 'keypad' as NUMPAD,
  POWER_SAVING: true,
  raceEventId: '',
  runnerListFilter: FILTER_RUNNERS_TO.SEEN,
  station: '',
  skipTimeIn: false,
  skipTimeOut: false,
  showGrid: false,
};
export type SettingsState = typeof moduleState;
type ActionContext = RTActionContext<SettingsState>;

// GETTERS
const moduleGetters = {};

// MUTATIONS
const moduleMutations = {
  ...make.mutations(moduleState),
};

// ACTIONS
const moduleActions = {
  ...make.actions(moduleState),

  async setRace({commit, dispatch, state}: ActionContext, event: Event | null) {
    dispatch('setRaceEventId', event && event.id || null);
    if (!event) {
      dispatch('setStation', '');
      store.dispatch('station/setStation', null);
    } else {
      let station = await Station.getStation({stationId: state.station, eventId: event.id});
      store.dispatch('station/setStation', station);
    }
  },

  toggleDark({state, commit}: ActionContext, toIsDark?: boolean) {
    const newIsDark = toIsDark ?? !state.DARK_MODE;
    if (!newIsDark) {
      localStorage.setItem('dark', 'false');
      commit('DARK_MODE', false);
    } else {
      localStorage.setItem('dark', 'true');
      commit('DARK_MODE', true);
    }
    if (newIsDark === isDark()) {
      localStorage.removeItem('dark');
    }
    document.documentElement.classList.add(newIsDark ? 'native--dark' : 'native--light');
    document.documentElement.classList.remove(newIsDark ? 'native--light' : 'native--dark');
  },
};
export type Actions = ActionInterface<typeof moduleActions, SettingsState>;

function isDark() {
  return !!darkMQL?.matches;
}


function getDarkStatus() {
  if (localStorage.getItem('dark') === 'true') {
    return true;
  } else if (localStorage.getItem('dark') === 'false') {
    return false;
  }
  return isDark();
}

let listenForDarkThemeChangeHandler: ((this: MediaQueryList, ev: MediaQueryListEvent) => any) | undefined;
export function listenForDarkThemeChange(store: import('vuex').Store<RootState>) { // tslint:disable-line no-shadowed-variable
  if (listenForDarkThemeChangeHandler) { stopListening(); }
  listenForDarkThemeChangeHandler = function listenForDarkThemeChangeHandler(this: MediaQueryList, evt: MediaQueryListEvent) { // tslint:disable-line no-shadowed-variable
    store.commit('settings/DARK_MODE', getDarkStatus());
  };
  darkMQL.addEventListener('change', listenForDarkThemeChangeHandler);
  function stopListening() {
    if (!listenForDarkThemeChangeHandler) { return; }
    darkMQL.removeEventListener('change', listenForDarkThemeChangeHandler);
    listenForDarkThemeChangeHandler = (void 0);
  }
  return stopListening;
}

export default {
  namespaced: true,
  state: moduleState,
  mutations: moduleMutations,
  actions: moduleActions,
  getters: moduleGetters,
};


pathify.options.mapping = PATHIFY_MAPPING;
