export enum FILTER_RUNNERS_TO {
  // ALL = 'all',
  EXPECTING = 'expecting',
  IN_STATION = 'inStation',
  NEEDS_TRANSMISSION = 'needsTransmission',
  SEEN = 'seen',
}
export type NUMPAD = 'keypad' | 'dialpad';
