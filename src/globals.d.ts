declare const __USE_MONGODB__: boolean;
declare const __USE_INDEXEDDB__: boolean;
/** Global boolean if process.env.NODE_ENV === 'development' */
declare const isDev: boolean;