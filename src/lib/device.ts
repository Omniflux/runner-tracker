import { Plugins } from '@capacitor/core';

const { Device } = Plugins;
export const info = Device.getInfo();

export const isDev = process.env.NODE_ENV === 'development';
