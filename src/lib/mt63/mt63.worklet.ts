// tslint:disable no-console
// import { MT63Client, readyDfd as wasmReady } from '@/lib/mt63';

// import { Resampler } from '../resampler';
import { MT63Client, loadWasm, wasmModule } from '@/lib/mt63/workletShim';
import { downSample } from './downsample';
if (typeof window !== 'undefined') {
  (window as any).MT63Client = MT63Client;
}


interface AudioWorkletProcessor {
  readonly port: MessagePort;
  process(
    inputs: Float32Array[][],
    outputs: Float32Array[][],
    parameters: Record<string, Float32Array>,
  ): boolean;
}

declare var AudioWorkletProcessor: {
  prototype: AudioWorkletProcessor;
  new (options?: AudioWorkletNodeOptions): AudioWorkletProcessor;
};

declare function registerProcessor(
  name: string,
  processorCtor: (new (
    options?: AudioWorkletNodeOptions,
  ) => AudioWorkletProcessor) & {
    parameterDescriptors?: AudioParamDescriptor[];
  },
): void;
declare const sampleRate: number;

declare namespace globalThis {
  let wasmLoaded: boolean;
  let mtClient: MT63Client;
  let buffer: Float32Array;
  let bufferPtr: number;
}

// The larger the chunk size the less frequently we bother the main thread, but also
// this needs to be a multiple of 6 and ideally 128
const chunkSize = 6 * 128 * 16;
const resampledSize = Math.ceil(chunkSize*(8000/sampleRate));
class MT63AudioProcessor extends AudioWorkletProcessor {
  mtClient?: import('@/lib/mt63').MT63Client;

  active = true;
  loc = 0;
  buffer = new Float32Array(chunkSize);
  bufferPtr = -1;
  resampleBuffer = new Float32Array(resampledSize);
  resampleBufferPtr = -1;

  constructor() {
    super();

    const pThis = this;
    this.port.onmessage = function(ev) {
      pThis.onMessage(this, ev);
    };
    if (globalThis.wasmLoaded) {
      this.loadWasm();
    } else {
      this.port.postMessage({req: 'startup'});
    }
  }

  async loadWasm(binFile?: Uint8Array) {
    try {
      if (globalThis.wasmLoaded) {
        this.mtClient = globalThis.mtClient;
        this.buffer = globalThis.buffer;
        this.bufferPtr = globalThis.bufferPtr;
        return;
      } else if (!binFile) {
        throw new Error("Can't load wasm - no binary provided");
      }
      await loadWasm(binFile);
      globalThis.mtClient = this.mtClient = new MT63Client();
      const oldBuff = this.buffer;
      const newBufPtr = wasmModule.mod._malloc(chunkSize * 4);
      globalThis.buffer = this.buffer = wasmModule.mod.HEAPF32.subarray(newBufPtr/4, newBufPtr/4 + chunkSize);
      globalThis.bufferPtr = this.bufferPtr = newBufPtr;
      this.buffer.set(oldBuff); // Copy from the old buffer
      globalThis.wasmLoaded = true;
    } catch (err) {
      console.warn("Error loading wasm: ", err);
    }
  }

  onMessage(port: MessagePort, ev: MessageEvent<any>) {
    if (ev.data.binary) {
      this.loadWasm(ev.data.binary);
    } else if (ev.data.req === 'shutdown') {
      this.active = false;
    }
  }

  process(inputs: Float32Array[][], outputs: Float32Array[][], parameters: Record<string, Float32Array>) {
    const inp = inputs[0][0]; // first input, first channel

    let {loc} = this;
    const remaining = chunkSize - loc;
    if (inp.length >= remaining) {
      // we need to split the array;
      this.buffer.set(inp.subarray(0, remaining), loc);
      // const maxVal =  this.buffer.reduce((memo, cur) => Math.max(memo, cur), 0);
      // console.log("Sending data across the great divide...", maxVal);
      this.processAudio(this.buffer, this.bufferPtr, chunkSize);

      // Shouldn't actually need to make a new array, we can keep using the old one
      // this.buffer = new Float32Array(chunkSize);
      let remnantSize = inp.length - remaining;
      this.buffer.set(inp.subarray(remaining, inp.length), 0);
      loc = remnantSize;
    } else {
      // append to the array
      this.buffer.set(inp, loc);
      loc += inp.length;
    }
    this.loc = loc;

    return this.active;
  }

  processAudio(floatArr: Float32Array, floatPtr: number, len: number) {
    if (this.mtClient && wasmModule) {

      // We're using a buffer which was allocated in memory used by wasm, so
      // we can just send a pointer directly to it. w00t!
      let res = wasmModule._processResampleMT63Rx(floatPtr, sampleRate, len);
      if (res?.length) {
        this.port.postMessage({decoded: res});
      }
    } else {
      // let ReSamp = new Resampler(sampleRate, 8000, 1, floatArr);
      const size = downSample(floatArr, len, sampleRate, 8000, this.resampleBuffer);
      // len = ReSamp.resampler(len);
      floatArr = this.resampleBuffer.subarray(0, size);
      this.port.postMessage({audioBuffer: floatArr, sampleRate: 8000});
    }
  }

}

registerProcessor('mt63-processor', MT63AudioProcessor);
