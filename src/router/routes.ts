import Entries from './views/Entries.vue';
import Grid from './views/Grid.vue';
import InputTimes from './views/InputTimes.vue';
import RaceEventPicker from './views/RaceEventPicker.vue';
import RunnerDetails from './views/RunnerDetails.vue';
import StationPicker from './views/StationPicker.vue';
import Transmit from './views/Transmit.vue';
import ViewTransmission from './views/ViewTransmission.vue';
import { RouteConfig } from 'vue-router';

export const ROUTES = {
  inputTimes: {
    path: '/input-times',
    name: 'inputTimes',
    component: InputTimes,
  } as RouteConfig,
  runnerDetails: {
    path: '/runners/:bibNumber',
    name: 'runnerDetails',
    component: RunnerDetails,
    props: (route) => {
      return { bibNumber: Number(route.params.bibNumber) };
    },
  } as RouteConfig,
  entries: {
    path: '/entries',
    name: 'entries',
    component: Entries,
  } as RouteConfig,
  transmit: {
    path: '/transmit',
    name: 'transmit',
    component: Transmit,
  } as RouteConfig,
  viewTransmission: {
    path: '/transmit/:hash',
    name: 'viewTransmission',
    component: ViewTransmission,
    props: true,
  } as RouteConfig,
  raceEventPicker: {
    path: '/settings/raceEventPicker',
    name: 'raceEventPicker',
    component: RaceEventPicker,
  } as RouteConfig,
  stationPicker: {
    path: '/settings/stationPicker',
    name: 'stationPicker',
    component: StationPicker,
  } as RouteConfig,
  grid: {
    path: '/grid',
    name: 'grid',
    component: Grid,
  } as RouteConfig,
  home: {
    path: '/',
    name: 'home',
    redirect: '/input-times',
  } as RouteConfig,
};

export default [
  ROUTES.inputTimes,
  ROUTES.runnerDetails,
  ROUTES.entries,
  ROUTES.transmit,
  ROUTES.viewTransmission,
  ROUTES.raceEventPicker,
  ROUTES.stationPicker,
  ROUTES.home,
  ROUTES.grid,
] as RouteConfig[];
