import Vue from 'vue';
import VueRouter, { Route } from 'vue-router';
import routes, { ROUTES } from './routes';
import store from '@/store';
import * as S from '@/store/settings/module';

Vue.use(VueRouter);

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes,
  // Use the HTML5 history API (i.e. normal-looking routes)
  // instead of routes with hashes (e.g. example.com/#/about).
  // This may require some server configuration in production:
  // https://router.vuejs.org/en/essentials/history-mode.html#example-server-configurations
  mode: 'history',
  // Simulate native-like scroll behavior when navigating to a new
  // route and using back/forward buttons.
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  },
});
router.beforeEach(async (routeTo, routeFrom, next) => {
  // If no raceEvent/Station is selected route to the race event picker
  const raceId = store.state.settings.raceEventId;
  const station = store.state.station.station;
  if (!raceId || !station) {
    if (
      routeTo.name === ROUTES.raceEventPicker.name ||
      (routeTo.name === ROUTES.stationPicker.name && raceId)
    ) { return next(); }
    return next({ name: ROUTES.raceEventPicker.name });
  }
  next();
});

export default router;
