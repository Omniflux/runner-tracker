import { Plugins } from '@capacitor/core';
import { Amp, Deamp } from '@hamstudy/flamp';
import { FileUpdateEvent, NewFileEvent, FileCompleteEvent, File } from '@hamstudy/flamp/dist/deamp';
import { i18next } from '@/locales/i18next';

import formatDate from '@/lib/formatDate';
import router from '@/router';
import { ROUTES } from '@/router/routes';
import { MT63Client, readyDfd as wasmReady, wasmBinary } from '@/lib/mt63';
(window as any).MT63Client = MT63Client;
import MT63WorkletPath from 'worklet-loader!@/lib/mt63/mt63.worklet.ts';
import { Entry, IEntryDoc } from '@/models/entry';
import { Participant, IParticipantDoc } from '@/models/participant';
import { Event, IEventDoc} from '@/models/event';
import { Station, IStationDoc } from '@/models/station';
import { Transmission, FilenameRegex } from '@/models/transmission';
import store from '@/store';
import { SNACKBAR } from '@/store/transmit/types';

export const webAudioCtx: AudioContext = new ((window as any).AudioContext || (window as any).webkitAudioContext)();

import {
  AudioInputPluginConfig,
  checkMicrophonePermission,
  getMicrophonePermission,
} from './cordova-plugin-audioinput';

const { StatusBar, Device} = Plugins;

const SECONDS = 1000;
let USE_MOBILE_NATIVE_AUDIO = !navigator.mediaDevices && !!(window as any).audioinput;

const audioConfig: AudioInputPluginConfig = {
  channels: 1,
  sampleRate: ((window as any).audioinput && audioinput.SAMPLERATE.TELEPHONE_8000Hz) || 8000,
  bufferSize: 8000,
  format: "PCM_16BIT",
  audioSourceType: ((window as any).audioinput && audioinput.AUDIOSOURCE_TYPE.MIC),
  normalize: true,
  streamToWebAudio: false,
};

async function saveTransmission(file: File) {
  const eventId: string = store.state.settings.raceEventId;
  const rx = (await Transmission.getRxDoc(file.hash, eventId)) || new Transmission(eventId, {txOrRx: 'rx', hash: file.hash, blocks: {}});
  const blocks: Amp['blocks'] = {};
  for (const block of file.headerBlocks) {
    rx.blocks[block.keyword as any] = block;
    blocks[block.keyword as any] = block;
  }
  for (const key of Object.keys(file.dataBlock)) {
    const block = file.dataBlock[key as any];
    if (!block) { continue; } // Not possible, but makes typescript happy
    rx.blocks[key as any] = block;
    blocks[block.keyword as any] = block;
  }
  for (const key of (Object.keys(rx.blocks))) {
    const block = rx.blocks[key as any];
    if (!(key in blocks)) {
      file.addBlock(block);
      blocks[key as any] = block;
    }
  }
  await rx.save();
}
class ReceiveService {
  audioLevel = 0;
  recentBuffer = '';
  hasMicPerm = false;
  lastReceivedAt: number = 0;
  listening = false;
  hearing = false;
  deamp: Deamp = new Deamp();
  setNotHearing?: NodeJS.Timer;

  private audioCtx!: AudioContext;
  private audioTrack?: MediaStreamTrack;
  private mtClient: MT63Client = null as any;


  constructor() {
    Promise.all([
      this.initialize(),
      this.initializeDeamp(),
    ]).then(() => {
      if (!store.state.settings.POWER_SAVING) {
        this.listen();
      }
    });
  }

  initializeDeamp(deamp?: Deamp) {
    if (deamp) { this.deamp = deamp; }
    this.deamp.newFileEvent.on(async (evt: NewFileEvent) => {
      let file = this.deamp.getFile(evt.hash);
      store.dispatch('transmit/setSnackbar', {
        message: i18next.t('receive.incoming', {hash: evt.hash}),
        timeout: 2 * SECONDS,
      } as SNACKBAR);
      await saveTransmission(file);
      await store.dispatch('transmit/updateHistoryList');
    });
    this.deamp.fileUpdateEvent.on(async (evt: FileUpdateEvent) => {
      let file = this.deamp.getFile(evt.hash);
      await saveTransmission(file);
      await store.dispatch('transmit/updateHistoryList');
    });
    this.deamp.fileCompleteEvent.on(async (evt: FileCompleteEvent) => {
      let file = this.deamp.getFile(evt.hash);
      await saveTransmission(file);
      await store.dispatch('transmit/updateHistoryList');
      let contents: string | undefined = (void 0);
      try {
        contents = file.getContent() || (void 0);
      } catch (e) {
        console.error(e); // tslint:disable-line no-console
      }
      store.dispatch('transmit/setSnackbar', {
        action: {
          text: i18next.t(file.name && FilenameRegex.message.test(file.name) ? 'receive.view' : 'receive.importNow'),
          callback: async () => {
            try {
              if (file.name && FilenameRegex.message.test(file.name)) {
                router.push({name: ROUTES.viewTransmission.name, params: {hash: evt.hash}});
              } else if (file.name && FilenameRegex.entry.test(file.name) && contents) {
                await this.parseStationCSV(file.name, contents);
              } else if (file.name && FilenameRegex.json.test(file.name) && contents) {
                await this.importData(contents, file.name, store.state.settings.raceEventId);
              } else if (file.name && FilenameRegex.drops.test(file.name) && contents) {
                await this.importDrops(contents);
              } else if (contents) {
                store.dispatch('transmit/setSnackbar', { action: { i18next: 'receive.importUnknown' } });
                await this.parseRaceData(contents, file.name, false);
              }
            } catch (e) {
              alert(e);
            }
          },
        },
        message: i18next.t('receive.historyDetails', {
          0: file.name,
          1: formatDate(new Date(), 'MM/DD HH:mm'),
          interpolation: { escapeValue: false },
        }),
        timeout: 0,
      } as SNACKBAR);
      this.deamp.popFile(evt.hash);
    });
  }

  async initialize() {
    const [deviceInfo] = await Promise.all([
      Device.getInfo(),
      wasmReady,
    ]);
    this.mtClient = new MT63Client();
    if (deviceInfo.platform === 'ios') { USE_MOBILE_NATIVE_AUDIO = true; }

    if (USE_MOBILE_NATIVE_AUDIO) {
      this.hasMicPerm = await checkMicrophonePermission();

      window.addEventListener('audioinput', (evt: any) => this.onAudioInput(evt.data, evt.data.length), false);
      window.addEventListener('audioinputerror', this.onAudioInputError.bind(this) as any, false);

      audioinput.initialize(audioConfig, () => {
        this.audioCtx = audioinput.getAudioContext();
      });
    } else {
      this.audioCtx = webAudioCtx;
    }
  }

  stopListening() {
    if (USE_MOBILE_NATIVE_AUDIO) {
      audioinput.stop(() => {
        this.listening = false; // This callback isn't being called... why?
      });
      this.listening = false; // Only because the callback isn't being called
      if (StatusBar) {
        StatusBar.setBackgroundColor({color: '#424242'}).catch(() => void 0);
      }
    } else if (this.audioTrack) {
      this.audioTrack.stop();
      // No need to set listening to false here because when the stream goes inactive it will do it
    } else {
      this.listening = false;
    }
  }

  onAudioInput(floatArr: Float32Array, len: number, sampleRate?: number) {
    if (this.lastReceivedAt &&
      (Date.now() - this.lastReceivedAt > 30 * SECONDS) &&
      store.state.settings.POWER_SAVING
    ) { this.stopListening(); }

    if (Transmission.activeTransmission?.transmitting) { return; }
    let res = this.mtClient.processAudio(floatArr, len, sampleRate);
    if (res.length) {
      this.onDataReceived(res);
    }
  }

  onDataReceived(res: string) {
    console.debug("Received:", res); // tslint:disable-line no-console
    this.recentBuffer += res;
    this.recentBuffer = this.recentBuffer.slice(this.recentBuffer.length - 500);
    this.deamp.ingestString(res);
    this.lastReceivedAt = Date.now();
    if (this.setNotHearing) {
      clearTimeout(this.setNotHearing);
    }
    this.hearing = true;
    this.setNotHearing = setTimeout(() => this.hearing = false, 1 * SECONDS);
  }

  onAudioInputError(evt: ErrorEvent) {
    // TODO: figure out what to do in this case??
    console.error("Couldn't process audio input", JSON.stringify(evt)); // tslint:disable-line no-console
  }

  async listen(deamp?: Deamp) {
    if (!USE_MOBILE_NATIVE_AUDIO) {
      if (this.audioCtx.state === 'closed') {
        this.audioCtx = new ((window as any).AudioContext || (window as any).webkitAudioContext)();
      } else if (this.audioCtx.state === 'suspended') {
        await this.audioCtx.resume();
      }
    }
    if (this.listening) { return; }
    this.deamp.clearBuffer();
    this.lastReceivedAt = 0;
    if (deamp) {
      this.initializeDeamp(deamp);
    }
    if (USE_MOBILE_NATIVE_AUDIO && !this.hasMicPerm) {
      try {
        // This will throw an exception if it fails
        this.hasMicPerm = await getMicrophonePermission();
      } catch(e) {
        console.error('User has not given permission to access mic', e); // tslint:disable-line no-console
        alert("This app does not have permission to access the micropohone");
        return;
      }
    }

    this.deamp.clearBuffer();
    if (USE_MOBILE_NATIVE_AUDIO) {
      try {
        audioinput.start(audioConfig);
        this.listening = true;
        if (StatusBar) {
          StatusBar.setBackgroundColor({color: 'red'}).catch(() => void 0);
        }
      } catch (e) {
        console.error(e); // tslint:disable-line no-console
        alert(e);
        audioinput.stop();
      }
    } else {
      let isLocalhost = window.location.hostname === 'localhost' ||
        window.location.hostname === '127.0.0.1';
      if (window.location.protocol !== 'https:' && !isLocalhost) {
        alert('HTTPS is required for microphone access, and this site has no SSL cert yet. Sorry!');
      }
      let stream = await navigator.mediaDevices.getUserMedia({audio: true});
      this.audioTrack = stream.getAudioTracks()[0];
      let microphone = this.audioCtx.createMediaStreamSource(stream);
      const [wasmBin] = await Promise.all([
        wasmBinary,
        waitForMe,
      ]);
      const mt63Node = new MT63Node(this.audioCtx, {mtClient: this.mtClient});
      mt63Node.connect(this.audioCtx.destination);
      mt63Node.port.onmessage = (e) => {
        if (e.data.req === 'startup') {
          mt63Node.port.postMessage({binary: wasmBin});
          return;
        } else if (e.data.decoded) {
          this.onDataReceived(e.data.decoded as string);
          return;
        } else if (e.data.audioBuffer) {
          const audioData = e.data.audioBuffer as Float32Array;
          const sampleRate = e.data.sampleRate as number;
          // console.log("Rcvd -- Sample rate:", sampleRate, " and max: ", audioData.reduce((memo, cur) => Math.max(memo, cur), 0));
          this.onAudioInput(audioData, audioData.length, sampleRate);
          // let res = this.mtClient.processAudio(audioData, audioData.length, sampleRate);
        }
      };
      stream.addEventListener('inactive', () => {
        this.listening = false;
        mt63Node.port.postMessage({req: 'shutdown'});
        if (StatusBar) {
          StatusBar.setBackgroundColor({color: '#424242'}).catch(() => void 0);
        }
      }, {once: true});
      microphone.connect(mt63Node);
      this.listening = true;
      if (StatusBar) {
        StatusBar.setBackgroundColor({color: 'red'}).catch(() => void 0);
      }
    }
  }
  async importDrops(body: string) {
    // Body should be a bunch of lines containing
    // bibNumber,droppedStation,droppedReason
    const eventId: string = store.state.settings.raceEventId;
    for (const line of body.split('\n')) {
      const [bibNumber, stationNumber, reason] = line.split(',');
      Promise.all([
        Participant.getParticipant(eventId, Number(bibNumber)),
        Station.getStation({eventId, stationNumber: Number(stationNumber)}),
      ]).then(([participant, station]) => {
        if (!participant || !station) { return; }
        participant.drop({reason, station});
        return participant.save();
      });
    }
  }

  async parseStationCSV(filename: string, body: string) {
    const promises: Array<Promise<any>> = [];
    let stationNumber = parseInt(filename.split('-')[1], 10);
    // TODO ask for what station this file should be stored
    if (stationNumber !== 0 && !stationNumber || isNaN(stationNumber)) { return alert("Received file for unknown station"); }
    const eventId: string = store.state.settings.raceEventId;
    let station = await Station.getStation({eventId, stationNumber});
    if (!station) {
      station = new Station(eventId, {stationNumber});
      await station.save();
    }
    const stationId = station.id;
    const lines = body.split('\n');
    let metadata: {[key: string]: any} = {}; // file may have JSON metadata
    try {
      metadata = JSON.parse(lines[0]);
      if (typeof metadata === 'object' && !Array.isArray(metadata)) {
        lines.shift(); // remove the metadata line from the lines array
      }
    } catch (e) {} // tslint:disable-line no-empty
    const epoch = metadata.epoch || 0;
    for (let line of lines) {
      line = line?.trim();
      if (!line) { continue; } // Don't parse empty lines in the file
      let [bibNum, timeInStr, timeOutStr, dnfReason] = line.split(',');
      const markDidNotFinish = dnfReason !== (void 0);
      if (isNaN(Number(bibNum))) { throw new Error('CSV is corrupt, expected bibnumber to be a number'); }
      if (isNaN(Number(timeInStr))) { throw new Error('CSV is corrupt, expected timeInStr to be a number'); }
      if (isNaN(Number(timeOutStr))) { throw new Error('CSV is corrupt, expected timeOutStr to be a number'); }
      let promise = (async () => {
        let participant = await Participant.getParticipant(eventId, Number(bibNum));
        if (!participant) { participant = new Participant(eventId, {bibNumber: Number(bibNum)}); }
        if (markDidNotFinish) {
          participant.drop({reason: dnfReason, station: station as Station});
        } else if (participant.dnfStation === stationNumber) {
          // Received a transmission for a runner that was previously dropped at said station, but is no longer reporting so
          participant.undrop();
        }
        await participant.save();
        let entry = await Entry.getEntry(stationId, participant.id);
        if (!entry) { entry = new Entry(eventId, participant.id, stationId); }
        if (timeInStr && !entry.timeIn) {
          entry.timeIn = new Date((epoch + Number(timeInStr)) * 1000 * 60);
        }
        if (timeOutStr && !entry.timeOut) {
          entry.timeOut = new Date((epoch + Number(timeOutStr)) * 1000 * 60);
        }
        await entry.save();
      })();
      promises.push(promise);
    }
    store.dispatch('transmit/setSnackbar', {
      i18next: 'receive.imported',
    } as SNACKBAR);
    await Promise.all(promises);
    return {
      runnerCount: body.split('\n').filter(l => !!l).length,
      stationNumber,
    };
  }
  async parseRaceData(body: string, filename?: string, createNewEvent = true) {
    let promises: Array<Promise<any>> = [];
    if (!body || typeof body !== 'string') { return; }
    // Import event data into current event event or create a new one
    const eventInfo: Partial<Event> & {name: string} = {
      id: !createNewEvent && store.state.settings.raceEventId || (void 0),
      name: filename || 'Event',
    };
    if (body.startsWith('race,')) {
      // race,name,startDate,endDate,id
      let [x, name, startDate, endDate, id] = body.split('\n')[0].trim().split(','); // tslint:disable-line:variable-name
      if (name) {
        eventInfo.name = name;
      }
      if (startDate && Date.parse(startDate)) {
        eventInfo.startDate = new Date(startDate);
      }
      if (endDate && Date.parse(endDate)) {
        eventInfo.endDate = new Date(endDate);
      }
      if (id) {
        eventInfo.id = id;
      }
    }
    let event = new Event(eventInfo);
    await event.save();
    for (let line of body.split('\n')) {
      line = line.trim();
      // s,stationNumber,name,distance
      if (line.startsWith('s,')) {
        let [x, sn, name, distance] = line.split(',');
        if (isNaN(Number(sn))) {
          throw new Error("Error parsing station number from data");
        }
        const stationNumber = Number(sn);
        let promise = Station.getStation({eventId: event.id, stationNumber}).then(station => {
          if (!station) { station = new Station(event.id, {stationNumber}); }
          station.name = name;
          station.distance = parseFloat(distance) || (void 0);
          return station.save();
        });
        promises.push(promise);
      // r,bibNumber,firstName,lastName,age,sex,note,team,home
      } else if (line.startsWith('r,')) {
        let [x,bibNumber,firstName,lastName,age,sex,note,team,home] = line.split(',');
        if (isNaN(Number(bibNumber))) {
          throw new Error("Error parsing runner data from data"); // tslint:disable-line
        }
        let promise = Participant.getParticipant(event.id, Number(bibNumber)).then(runner => {
          if (!runner) { runner = new Participant(event.id, {bibNumber: Number(bibNumber)}); }
          runner.firstName = firstName || runner.firstName;
          runner.lastName = lastName || runner.lastName;
          runner.age = age || runner.age;
          runner.sex = sex as 'M' | 'F' || runner.sex;
          runner.note = note || runner.note;
          runner.team = team || runner.team;
          runner.home = home || runner.home;
          return runner.save();
        });
        promises.push(promise);
      }
    }
    await Promise.all(promises);
    store.dispatch('transmit/setSnackbar', {
      i18next: 'receive.imported',
    } as SNACKBAR);
  }

  async importData(data: IImportData | string, filename: string, eventId?: string) {
    if (!data) { return; }
    if (typeof data === 'string') {
      try {
        data = JSON.parse(data) as IImportData;
      } catch(e) {
        console.error(e); // tslint:disable-line no-console
        return;
      }
    }
    data.id = eventId || data.id;
    const event = await importEvent(data, filename);

    const stationMap: {[stationNumber: string]: Station} = {};
    const stationPromises: Array<Promise<any>> = [];
    for (const sData of data.stations || []) {
      if (typeof sData.stationNumber !== 'number') { continue; }
      const stationPromise = (async () => {
        const s = await importStation(event.id, sData);
        stationMap[s.stationNumber] = s;
      })();
      stationPromises.push(stationPromise);
    }

    const promises: Array<Promise<any>> = [];
    for (const rData of data.participants || data.runners || []) {
      if (!rData.bibNumber) { continue; }
      let promise = (async () => {
        const participant = await importParticipant(event.id, rData);
        let ePromises: Array<Promise<any>> = [];
        const entrys = rData.entrys;
        if (entrys) {
          await Promise.all(stationPromises);
          for (const stationNumber of Object.keys(entrys || {})) {
            const station = stationMap[stationNumber];
            if (!station) { continue; }
            let ePromise = (async () => {
              const eData = entrys[stationNumber];
              const entry = await importEntry(event.id, station.id, participant.id, eData);
            })();
            await ePromise;
            ePromises.push(ePromise);
          }
        }
        await Promise.all(ePromises);
      })();
      await Promise;
      promises.push(promise);
    }

    await Promise.all(promises);
    store.dispatch('transmit/setSnackbar', {
      i18next: 'receive.imported',
    } as SNACKBAR);
  }
}

async function importEvent(eventData: Partial<IEventDoc>, fallbackName: string) {
  const event = eventData.id && (await Event.getEvent(eventData.id)) || new Event({id: eventData.id, name: ''});
  event.name = eventData.name || event.name || fallbackName;
  event.startDate = eventData.startDate && new Date(eventData.startDate) || event.startDate;
  event.endDate = eventData.endDate && new Date(eventData.endDate) || event.endDate;
  try {
    await event.save();
  } catch (e) {
    console.error(e); // tslint:disable-line no-console
    throw e;
  }
  return event;
}

async function importStation(eventId: string, stationData: Partial<IStationDoc> & { stationNumber: number; }) {
  const station = (await Station.getStation({eventId, stationNumber: stationData.stationNumber})) || new Station(eventId, {stationNumber: stationData.stationNumber});
  station.name = stationData.name || station.name || '';
  if ('distance' in stationData) { station.distance = parseFloat(String(stationData.distance)); }
  if ('stationNumberDisplayed' in stationData) { station.stationNumberDisplayed = stationData.stationNumberDisplayed || ''; }
  if (!station.id && !!stationData.id) { station.id = stationData.id; }
  try {
    await station.save();
  } catch (e) {
    console.error(e); // tslint:disable-line no-console
    throw e;
  }
  return station;
}

async function importParticipant(eventId: string, participantData: Partial<IParticipantDoc> & {bibNumber: number; entrys?: {[stationNumber: string]: Partial<IEntryDoc>}}) {
  const runner = (await Participant.getParticipant(eventId, participantData.bibNumber)) || new Participant(eventId, {bibNumber: participantData.bibNumber});
  runner.age = participantData.age || runner.age;
  runner.dnfReason = participantData.dnfReason || runner.dnfReason;
  if (typeof participantData.dnfStation === 'number') {
    runner.dnfStation = participantData.dnfStation;
  }
  runner.firstName = participantData.firstName || runner.firstName;
  runner.lastName = participantData.lastName || runner.lastName;
  runner.home = participantData.home || runner.home;
  runner.note = participantData.note || runner.note;
  runner.sex = participantData.sex || runner.sex;
  runner.team = participantData.team || runner.team;
  try {
    await runner.save();
  } catch (e) {
    console.error(e); // tslint:disable-line no-console
    throw e;
  }
  return runner;
}

async function importEntry(eventId: string, stationId: string, participantId: string, entryData: Partial<IEntryDoc>) {
  const entry = (await Entry.getEntry(stationId, participantId)) || new Entry(eventId, participantId, stationId);
  if (entryData.timeIn === null) {
    entry.timeIn = null;
  } else if (entryData.timeIn && Date.parse(String(entryData.timeIn))) {
    entry.timeIn = new Date(entryData.timeIn);
  }
  if (entryData.timeOut === null) {
    entry.timeOut = null;
  } else if (entryData.timeOut && Date.parse(String(entryData.timeOut))) {
    entry.timeOut = new Date(entryData.timeOut);
  }
  try {
    await entry.save();
  } catch (e) {
    console.error(e); // tslint:disable-line no-console
    throw e;
  }
  return entry;
}

export interface IImportData extends Partial<IEventDoc> {
  runners?: Array<Partial<IParticipantDoc> & { bibNumber: number; entrys?: {[stationNumber: string]: Partial<IEntryDoc>}}>;
  participants?: Array<Partial<IParticipantDoc> & { bibNumber: number; entrys?: {[stationNumber: string]: Partial<IEntryDoc>}}>;
  stations?: Array<Partial<IStationDoc> & { stationNumber: number; }>;
}

// Question where would you handle onDestroy?
export default new ReceiveService();


(window as any).Amp = Amp;
(window as any).Deamp = Deamp;

class MT63Node extends AudioWorkletNode { // tslint:disable max-classes-per-file
  constructor(context: AudioContext, options: any) {
    super(context, 'mt63-processor', options);
  }
}

const waitForMe = (async () => {
  try {
    // console.log(MT63WorkletPath);
    await webAudioCtx.audioWorklet.addModule(MT63WorkletPath);
    // console.log(MT63WorkletPath, 'loaded');
  } catch (e) {
    console.log('>>', e); // tslint:disable-line no-console
  }
})();
