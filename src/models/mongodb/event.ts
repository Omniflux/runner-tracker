import Base from '../base/event';
import { db, Types, Doc } from '@/models/mongodb/db';
import { collection as participantCollection } from './participant';
import { collection as stationCollection } from './station';
import { txCollection, rxCollection } from './transmission';

const collection = db.collection('raceEvents');

export class Event extends Base {
  static async getEvent(id: string): Promise<Event | null> {
    if (!id) { return null; }
    let doc = await collection.findOne<Doc<Event>>({
      _id: new Types.ObjectId(id),
    });
    return doc === null ? null : dbConverter.fromDB(doc);
  }
  static async getEvents(): Promise<Event[]> {
    const cursor = await collection.find<Doc<Event>>();
    const events: Event[] = [];
    let doc: Doc<Event> | null;
    while (doc = await cursor.next()) { // tslint:disable-line:no-conditional-assignment
      events.push(dbConverter.fromDB(doc));
    }
    return events;
  }
  static async delete(event: Event) {
    const eventId = new Types.ObjectId(event.id);
    await Promise.all([
      collection.deleteOne({_id: eventId}),
      stationCollection.deleteMany({eventId}),
      participantCollection.deleteMany({eventId}),
      txCollection.deleteMany({eventId}),
      rxCollection.deleteMany({eventId}),
    ]);
  }

  async save() {
    const id = new Types.ObjectId(this.id);
    await collection.updateOne(
      {_id: id},
      {$set: dbConverter.toDB(this)},
      {upsert: true},
    );
    this.id = id.toHexString();
  }
}
export default Event;

// DB data converter
const dbConverter = {
  toDB(x: Event) {
    return {
      name: x.name,
      startDate: x.startDate,
      endDate: x.endDate,
    };
  },
  fromDB(doc: Doc<Event>) {
    const startDate = doc.startDate ? new Date(doc.startDate) : (doc as any).date ? new Date((doc as any).date) : (void 0);
    const endDate = doc.endDate ? new Date(doc.endDate) : (void 0);
    const id = doc._id.toHexString();
    return new Event({id, name: doc.name, startDate, endDate});
  },
};
