import Base, { IEntryDoc } from '../base/entry';
import { Cursor } from '@hamstudy/mongodb-mobile-client';
import { db, Types, Doc, initProm } from '@/models/mongodb/db';

export const collection = db.collection('entrys');

(async () => {
  await initProm;
  collection.createIndex({stationId: 1, participantId: 1}, {unique: true});
  collection.createIndex({eventId: 1});
  collection.createIndex({participantId: 1});
})();

export class Entry extends Base {
  static async getEntry(id: string): Promise<Entry | null>;
  static async getEntry(stationId: string, participantId: string): Promise<Entry | null>; // tslint:disable-line unified-signatures
  static async getEntry(id: string, participantId?: string): Promise<Entry | null> {
    let doc: Doc<Entry> | null = null;
    if (!participantId) {
      doc = await collection.findOne<Doc<Entry>>({_id: new Types.ObjectId(id)});
    } else {
      doc = await collection.findOne<Doc<Entry>>({stationId: new Types.ObjectId(id), participantId: new Types.ObjectId(participantId)});
    }
    return doc ? dbConverter.fromDB(doc) : null;
  }
  static async getEntrys({participantId, stationId, eventId, sinceDate}: Partial<{participantId: string; stationId: string; eventId: string; sinceDate: Date}>): Promise<Entry[]> {
    let cursor: Cursor<Doc<Entry>> | null = null;
    if (participantId) {
      cursor = collection.find<Doc<Entry>>({participantId: new Types.ObjectId(participantId)});
    } else if (stationId) {
      if (sinceDate) {
        cursor = collection.find<Doc<Entry>>({stationId: new Types.ObjectId(stationId), modified: {$gt: sinceDate}});
      } else {
        cursor = collection.find<Doc<Entry>>({stationId: new Types.ObjectId(stationId)});
      }
    } else if (eventId) {
      cursor = collection.find<Doc<Entry>>({eventId: new Types.ObjectId(eventId)});
    }
    if (!cursor) { throw new Error('No cursor, but this shouldn\'t happen!'); }
    const entrys: Entry[] = [];
    let doc: Doc<Entry> | null;
    while (doc = await cursor.next()) { // tslint:disable-line:no-conditional-assignment
      entrys.push(dbConverter.fromDB(doc));
    }
    return entrys;
  }

  static async delete(entry: Entry) {
    const id = new Types.ObjectId(entry.id);
    await collection.deleteOne({_id: id});
  }

  async save() {
    const id = new Types.ObjectId(this.id);
    await collection.updateOne(
      {_id: id},
      {$set: dbConverter.toDB(this)},
      {upsert: true},
    );
    this.id = id.toHexString();
  }
}
export default Entry;


// DB data converter
const dbConverter = {
  toDB(x: Entry) {
    return {
      eventId: new Types.ObjectId(x.eventId),
      participantId: new Types.ObjectId(x.participantId),
      stationId: new Types.ObjectId(x.stationId),
      modified: new Date(),
      timeIn: x.timeIn,
      timeOut: x.timeOut,
    };
  },
  fromDB(doc: Doc<Entry>) {
    const id = doc._id.toHexString();
    const eventId = String(doc.eventId);
    const participantId = String(doc.participantId);
    const stationId = String(doc.stationId);
    const timeIn = doc.timeIn ? new Date(doc.timeIn) : null;
    const timeOut = doc.timeOut ? new Date(doc.timeOut) : null;
    const modified = doc.modified && new Date(doc.modified);
    return new Entry(eventId, participantId, stationId, {id, timeIn, timeOut, modified});
  },
};
