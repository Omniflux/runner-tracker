import Base from '../base/participant';
import { db, Types, initProm, Doc, migrations } from '@/models/mongodb/db';

export const collection = db.collection('runners');

(async () => {
  await initProm;
  await migrations;
  collection.createIndex({eventId: 1, bibNumber: 1}, {unique: true});
})();

export class Participant extends Base {
  static async getParticipant(eventId: string, bibNumber: number) {
    let doc = await collection.findOne<Doc<Participant>>({
      eventId: new Types.ObjectId(eventId),
      bibNumber,
    });
    return doc === null ? null : dbConverter.fromDB(doc);
  }
  static async getParticipants(eventId: string, ids?: string[]) {
    const query: any = {
      eventId: new Types.ObjectId(eventId),
    };
    if (ids) {
      query._id = { $in: ids.map(id => new Types.ObjectId(id)) };
    }
    const cursor = await collection.find<Doc<Participant>>(query);
    const participants: Participant[] = [];
    let doc: Doc<Participant> | null;
    while (doc = await cursor.next()) { // tslint:disable-line:no-conditional-assignment
      participants.push(dbConverter.fromDB(doc));
    }
    return participants;
  }

  static async delete(participant: Participant) {
    const id = new Types.ObjectId(participant.id);
    await collection.deleteOne({_id: id});
  }

  async save() {
    const id = new Types.ObjectId(this.id);
    await collection.updateOne(
      {_id: id},
      {$set: dbConverter.toDB(this)},
      {upsert: true},
    );
    this.id = id.toHexString();
  }
}
export default Participant;

// DB data converter
const dbConverter = {
  toDB(x: Participant) {
    return Object.assign({}, x, {modified: new Date(), eventId: new Types.ObjectId(x.eventId)});
  },
  fromDB(doc: Doc<Participant>) {
    const eventId = String(doc.eventId);
    const id = doc._id.toHexString();
    return new Participant(eventId, Object.assign({}, doc, {id}));
  },
};
