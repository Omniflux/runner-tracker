import Base from '../base/station';
import { db, Types, Doc, initProm, migrations } from './db';
import { Cursor } from '@hamstudy/mongodb-mobile-client';

export const collection = db.collection('stations');

(async () => {
  await initProm;
  await migrations;
  collection.createIndex({eventId: 1}, {unique: false});
  collection.createIndex({eventId: 1, stationNumber: 1}, {unique: true});
})();


export class Station extends Base {
  // This is used to verify that the station id we have belongs to this event, if not it'll be null
  static async getStation(obj: {eventId: string; stationId?: string;}): Promise<Station | null>; // tslint:disable-line unified-signatures
  static async getStation(obj: {eventId: string; stationNumber: number}): Promise<Station | null>; // tslint:disable-line unified-signatures
  static async getStation({eventId, stationId, stationNumber}: Partial<{eventId: string; stationId: string; stationNumber: number;}>): Promise<Station | null> {
    if (!(eventId || stationId)) { return null; }
    let doc: Doc<Station> | null = null;
    if (!eventId && stationId) {
      doc = await collection.findOne<Doc<Station>>({_id: new Types.ObjectId(stationId)});
    } else if (eventId && typeof stationNumber === 'number') {
      doc = await collection.findOne<Doc<Station>>({eventId: new Types.ObjectId(eventId), stationNumber});
    } else if (eventId && stationId) {
      doc = await collection.findOne<Doc<Station>>({_id: new Types.ObjectId(stationId), eventId: new Types.ObjectId(eventId)});
    }
    return doc ? dbConverter.fromDB(doc) : null;
  }

  static async getStations(eventId: string) {
    const cursor = collection.find<Doc<Station>>({eventId: new Types.ObjectId(eventId)});
    let stations: Station[] = [];
    let doc: Doc<Station> | null;
    while (doc = await cursor.next()) { // tslint:disable-line:no-conditional-assignment
      stations.push(dbConverter.fromDB(doc));
    }
    return stations;
  }
  static async delete(station: Station) {
    await collection.deleteOne({_id: new Types.ObjectId(station.id)});
  }

  async save() {
    const id = new Types.ObjectId(this.id);
    await collection.updateOne(
      {_id: id},
      {$set: dbConverter.toDB(this)},
      {upsert: true},
    );
    this.id = id.toHexString();
  }
}
export default Station;

// DB data converter
const dbConverter = {
  toDB(x: Station) {
    return {
      eventId: new Types.ObjectId(x.eventId),
      name: x.name,
      distance: x.distance,
      stationNumber: x.stationNumber,
      stationNumberDisplayed: x.stationNumberDisplayed,
    };
  },
  fromDB(doc: any) {
    const id = doc._id.toHexString();
    const eventId = String(doc.eventId);
    return new Station(eventId, Object.assign({}, doc, {id}));
  },
};
