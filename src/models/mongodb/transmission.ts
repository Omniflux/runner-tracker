// tslint:disable max-classes-per-file
import { Cursor } from '@hamstudy/mongodb-mobile-client';
import Base, { txDbName, rxDbName, ITransmissionDoc} from '../base/transmission';
import { db, Types, Doc, initProm } from './db';
import { Amp } from '@hamstudy/flamp';
import { Block } from '@hamstudy/flamp/dist/block';
import { LTypes, ControlWord } from '@hamstudy/flamp/dist/amp';

export const txCollection = db.collection(txDbName);
export const rxCollection = db.collection(rxDbName);

(async () => {
  await initProm;
  txCollection.createIndex({hash: 1, eventId: 1}, {unique: true});
  txCollection.createIndex({timestamp: 1});
  rxCollection.createIndex({hash: 1, eventId: 1}, {unique: true});
})();

async function getDoc(x: 'tx' | 'rx', hash: string, eventId?: string) {
  const collection = x === 'tx' ? txCollection : rxCollection;
  if (!hash) { return null; }
  let query: {hash: string; eventId?: Types.ObjectId} = { hash };
  if (eventId) {
    query.eventId = new Types.ObjectId(eventId);
  }
  let doc = await collection.findOne<Doc<Transmission>>(query);
  return doc === null ? null : dbConverter.fromDB(doc, x);
}
async function getDocs(x: 'tx' | 'rx', eventId: string) {
  const collection = x === 'tx' ? txCollection : rxCollection;
  const cursor = await collection.find<Doc<Transmission>>({eventId: new Types.ObjectId(eventId)});
  const docs: Transmission[] = [];
  let doc: Doc<Transmission> | null;
  while (doc = await cursor.next()) { // tslint:disable-line:no-conditional-assignment
    docs.push(dbConverter.fromDB(doc, x));
  }
  return docs;
}

export class Transmission extends Base {
  static async getLastEntryTXTimestamp(eventId: string): Promise<Date> {
    const cursor = await txCollection
      .find<Doc<Transmission>>({eventId: new Types.ObjectId(eventId), hasAllChanges: true})
      .sort({timestamp: -1})
      .limit(1)
    ;
    let doc = await cursor.next();
    await cursor.close();
    return doc ? dbConverter.fromDB(doc, 'tx').timestamp : new Date(0);
  }
  static async getTxDoc(hash: string, eventId?: string): Promise<Transmission | null> { return getDoc('tx', hash, eventId); }
  static async getTxDocs(eventId: string): Promise<Transmission[]> { return getDocs('tx', eventId); }
  static async getRxDoc(hash: string, eventId?: string): Promise<Transmission | null> { return getDoc('rx', hash, eventId); }
  static async getRxDocs(eventId: string): Promise<Transmission[]> { return getDocs('rx', eventId); }
  static async delete(x: Transmission): Promise<void> {
    const collection = x.txOrRx === 'tx' ? txCollection : rxCollection;
    await collection.deleteOne({hash: x.hash, eventId: new Types.ObjectId(x.eventId)});
  }

  async save(): Promise<void> {
    const collection = this.txOrRx === 'tx' ? txCollection : rxCollection;
    await collection.updateOne(
      {hash: this.hash, eventId: new Types.ObjectId(this.eventId)},
      {$set: dbConverter.toDB(this)},
      {upsert: true},
    );
  }
}
export default Transmission;

// DB data converter
const dbConverter = {
  toDB(x: Transmission) {
    let obj: any = {
      hash: x.hash,
      eventId: new Types.ObjectId(x.eventId),
      blocks: x.blocks,
      timestamp: x.timestamp,
    };
    if (x.entryCount) {
      obj.entryCount = x.entryCount;
    }
    if (x.hasAllChanges) {
      obj.hasAllChanges = true;
    }
    return obj;
  },
  fromDB(doc: Doc<Transmission>, txOrRx: 'tx' | 'rx') {
    const raceEventId = String(doc.eventId);
    const blocks: Amp['blocks'] = {};
    for (const key of Object.keys(doc.blocks) as Array<keyof Amp['blocks']>) {
      const b = doc.blocks[key as any];
      const keyword = b.keyword;
      const hash = b.hash;
      const data = b.data;
      switch(keyword) {
        case LTypes.DATA:
          blocks[key] = Block.MakeBlock({keyword, hash, data, blockNum: b.blockNum as number});
          break;
        case LTypes.CNTL:
          blocks[key] = Block.MakeBlock({keyword, hash, controlWord: b.controlWord as ControlWord});
          break;
        default:
          blocks[key] = Block.MakeBlock({keyword, hash, data});
      }
    }
    let x = new Transmission(raceEventId, {txOrRx, hash: doc.hash, blocks, entryCount: doc.entryCount});
    if (doc.hasAllChanges) { x.hasAllChanges = doc.hasAllChanges; }
    x.timestamp = new Date(doc.timestamp);
    return x;
  },
};
