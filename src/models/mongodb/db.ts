import { Plugins } from '@capacitor/core';
import { MongoDBMobileSource, setMongoMobilePlugin, Db, Types } from '@hamstudy/mongodb-mobile-client';

export {
  Types,
};

export type Doc<T> = {[P in keyof T]: T[P]} & {_id: Types.ObjectId};

const Device = Plugins.Device;
const MongoDBMobile = Plugins.MongoDBMobile as MongoDBMobileSource;
setMongoMobilePlugin(MongoDBMobile);


export const db = new Db('runnerTracker');
if (isDev) {
  (window as any).Db = Db;
  (window as any).db = db;
  (window as any).Types = Types;
}

export const initProm = MongoDBMobile.initDb()
  .catch(async err => {
    const deviceInfo = await Device.getInfo();
    if (deviceInfo.platform !== 'web') {
      alert(err);
    }
    return [{success: false}];
  })
;

export const migrations = Promise.all([
  (async () => {
    // Migration from 1.4.9 to 1.5.0
    await initProm;
    const participantCollection = db.collection('runners');
    const stationCollection = db.collection('stations');
    await Promise.all([
      participantCollection.dropIndex({raceEventId: 1, bibNumber: 1}),
      stationCollection.dropIndex({raceEventId: 1, stationNumber: 1}),
    ]);
    await Promise.all([
      participantCollection.updateMany({raceEventId: { $exists: true }}, {$rename: { raceEventId: 'eventId' }}),
      stationCollection.updateMany({raceEventId: { $exists: true }}, {$rename: { raceEventId: 'eventId' }}),
    ]);

    if ((await db.collections).find(c => c.collectionName === 'files')) {
      await db.dropCollection('files');
    }
  })(),
]);
