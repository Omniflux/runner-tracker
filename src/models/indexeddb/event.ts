import { IDBPDatabase } from 'idb';
import Base, { dbName as storeName, IEventDoc } from '../base/event';
import { dbProm, RunnerTrackerDB } from './db';
import { Types } from '@hamstudy/mongodb-mobile-client';
import { getEntryStore } from './entry';
import { getParticipantStore } from './participant';
import { getStationStore } from './station';
import { getTransmissionStore } from './transmission';
// import { collection as participantCollection } from './participant';
// import { collection as stationCollection } from './station';
// import { AmpFileCol } from '../transmission';

function getStore(db: IDBPDatabase<RunnerTrackerDB>) {
  const store = db.transaction(storeName, 'readwrite').objectStore(storeName);
  return store;
}

export class Event extends Base {
  static async getEvent(id: string): Promise<Event | null> {
    if (!id) { return null; }
    const store = getStore(await dbProm);
    let doc = await store.get(id);
    return doc ? dbConverter.fromDB(doc) : null;
  }
  static async getEvents(): Promise<Event[]> {
    const docs: Event[] = [];
    const store = getStore(await dbProm);
    let cursor = await store.openCursor();
    while (cursor) {
      docs.push(dbConverter.fromDB(cursor.value));
      cursor = await cursor.continue();
    }
    return docs;
  }
  static async delete(event: Event) {
    let promises: Array<Promise<any>> = [];

    // Delete entrys
    const entryStore = getEntryStore(await dbProm);
    let eCursor = await entryStore.index('eventId').openKeyCursor(IDBKeyRange.only(event.id));
    while (eCursor) {
      promises.push(entryStore.delete(eCursor.primaryKey));
      eCursor = await eCursor.continue();
    }

    // Delete participants
    const participantStore = getParticipantStore(await dbProm);
    let pCursor = await participantStore.index('eventId').openKeyCursor(IDBKeyRange.only(event.id));
    while (pCursor) {
      promises.push(participantStore.delete(pCursor.primaryKey));
      pCursor = await pCursor.continue();
    }

    // Delete stations
    const stationStore = getStationStore(await dbProm);
    let sCursor = await stationStore.index('eventId').openKeyCursor(IDBKeyRange.only(event.id));
    while (sCursor) {
      promises.push(stationStore.delete(sCursor.primaryKey));
      sCursor = await sCursor.continue();
    }

    // Delete Tranmissions
    const txStore = getTransmissionStore(await dbProm, 'tx');
    const rxStore = getTransmissionStore(await dbProm, 'rx');
    let txCursor = await txStore.index('eventId').openKeyCursor(IDBKeyRange.only(event.id));
    let rxCursor = await txStore.index('eventId').openKeyCursor(IDBKeyRange.only(event.id));
    while (txCursor) {
      promises.push(txStore.delete(txCursor.primaryKey));
      txCursor = await txCursor.continue();
    }
    while (rxCursor) {
      promises.push(rxStore.delete(rxCursor.primaryKey));
      rxCursor = await rxCursor.continue();
    }

    // Delete event
    const store = getStore(await dbProm);
    promises.push(store.delete(event.id));
    await Promise.all(promises);
  }

  async save() {
    const store = getStore(await dbProm);
    await store.put(dbConverter.toDB(this));
  }
}
export default Event;

// DB data converter
const dbConverter = {
  toDB(x: Event) {
    x.id = x.id || (new Types.ObjectId()).toHexString();
    const doc: IEventDoc = {
      id: x.id,
      name: x.name,
      startDate: x.startDate,
      endDate: x.endDate,
    };
    return doc;
  },
  fromDB(doc: IEventDoc) {
    const startDate = doc.startDate ? new Date(doc.startDate) : (doc as any).date ? new Date((doc as any).date) : (void 0);
    const endDate = doc.endDate ? new Date(doc.endDate) : (void 0);
    const id = doc.id;
    return new Event({id, name: doc.name, startDate, endDate});
  },
};
