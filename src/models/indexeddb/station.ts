import { IDBPDatabase } from 'idb';
import Base, { dbName as storeName, IStationDoc } from '../base/station';
import { dbProm, RunnerTrackerDB } from './db';
import { Types } from '@hamstudy/mongodb-mobile-client';
import { getEntryStore } from './entry';

export function getStationStore(db: IDBPDatabase<RunnerTrackerDB>) {
  return db.transaction(storeName, 'readwrite').objectStore(storeName);
}

export class Station extends Base {
  // This is used to verify that the station id we have belongs to this event, if not it'll be null
  static async getStation(obj: {eventId: string; stationId?: string;}): Promise<Station | null>; // tslint:disable-line unified-signatures
  static async getStation(obj: {eventId: string; stationNumber: number}): Promise<Station | null>; // tslint:disable-line unified-signatures
  static async getStation({eventId, stationId, stationNumber}: Partial<{eventId: string; stationId: string; stationNumber: number;}>): Promise<Station | null> {
    if (!(eventId || stationId)) { return null; }
    const store = getStationStore(await dbProm);
    let doc: IStationDoc | undefined;
    if (!eventId && stationId) {
      doc = await store.get(stationId);
    } else if (eventId && typeof stationNumber === 'number') {
      doc = await store.index('event-stationNum').get(IDBKeyRange.only([eventId, stationNumber]));
    } else if (eventId && stationId) {
      doc = await store.index('id-event').get(IDBKeyRange.only([stationId, eventId]));
    }
    return doc ? dbConverter.fromDB(doc) : null;
  }

  static async getStations(eventId: string) {
    const docs: Station[] = [];
    const store = (getStationStore(await dbProm)).index('eventId');
    let cursor = await store.openCursor(IDBKeyRange.only(eventId));
    while (cursor) {
      docs.push(dbConverter.fromDB(cursor.value));
      cursor = await cursor.continue();
    }
    return docs;
  }

  static async delete(station: Station) {
    let promises: Array<Promise<any>> = [];
    const entryStore = getEntryStore(await dbProm);
    let cursor = await entryStore.index('stationId').openKeyCursor(IDBKeyRange.only(station.id));
    while (cursor) {
      promises.push(entryStore.delete(cursor.primaryKey));
      cursor = await cursor.continue();
    }
    const store = getStationStore(await dbProm);
    promises.push(store.delete(station.id));
    await Promise.all(promises);
  }

  async save() {
    const store = getStationStore(await dbProm);
    await store.put(dbConverter.toDB(this));
  }
}
export default Station;

// DB data converter
const dbConverter = {
  toDB(x: Station) {
    x.id = x.id || (new Types.ObjectId()).toHexString();
    return {
      id: x.id,
      eventId: x.eventId,
      name: x.name,
      distance: x.distance,
      stationNumber: x.stationNumber,
      stationNumberDisplayed: x.stationNumberDisplayed,
    };
  },
  fromDB(doc: IStationDoc) {
    const raceEventId = String(doc.eventId);
    return new Station(raceEventId, Object.assign({}, doc));
  },
};
