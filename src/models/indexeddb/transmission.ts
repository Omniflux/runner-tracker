// tslint:disable max-classes-per-file
import { IDBPDatabase } from 'idb';
import Base, { txDbName, rxDbName, ITransmissionDoc} from '../base/transmission';
import { dbProm, RunnerTrackerDB } from './db';
import { Amp } from '@hamstudy/flamp';
import { Block } from '@hamstudy/flamp/dist/block';
import { LTypes, ControlWord } from '@hamstudy/flamp/dist/amp';
import { IDBPObjectStore } from 'idb';

export function getTransmissionStore(db: IDBPDatabase<RunnerTrackerDB>, x: 'tx' | 'rx') {
  const storeName = x === 'tx' ? txDbName : rxDbName;
  const store = db.transaction(storeName, 'readwrite').objectStore(storeName);
  return store;
}

async function getDoc(x: 'tx' | 'rx', hash: string, eventId?: string) {
  let store = getTransmissionStore(await dbProm, x);
  let doc: ITransmissionDoc | undefined;
  if (eventId) {
    doc = await store.index('event-hash').get(IDBKeyRange.only([eventId, hash]));
  } else {
    doc = await store.get(hash);
  }
  return doc ? dbConverter.fromDB(doc, x) : null;
}
async function getDocs(x: 'tx' | 'rx', eventId: string) {
    const docs: Transmission[] = [];
    let cursor = await (getTransmissionStore(await dbProm, x)).index('eventId').openCursor(IDBKeyRange.only(eventId));
    while (cursor) {
      docs.push(dbConverter.fromDB(cursor.value, x));
      cursor = await cursor.continue();
    }
    return docs;
}

export class Transmission extends Base {
  static async getLastEntryTXTimestamp(eventId: string): Promise<Date> {
    const store: IDBPObjectStore<RunnerTrackerDB, ["transmissionTX"], "transmissionTX"> = getTransmissionStore(await dbProm, 'tx') as any;
    let cursor = await store.index('event-timestamp').openCursor(IDBKeyRange.bound([eventId, new Date(0)], [eventId, new Date()]), 'prev');
    while (cursor) {
      const doc = cursor.value;
      if (doc.hasAllChanges) {
        return dbConverter.fromDB(doc, 'tx').timestamp;
      }
      cursor = await cursor.continue();
    }
    return new Date(0);
  }
  static async getTxDoc(hash: string, eventId?: string): Promise<Transmission | null> { return getDoc('tx', hash, eventId); }
  static async getTxDocs(eventId: string): Promise<Transmission[]> { return getDocs('tx', eventId); }
  static async getRxDoc(hash: string, eventId?: string): Promise<Transmission | null> { return getDoc('rx', hash, eventId); }
  static async getRxDocs(eventId: string): Promise<Transmission[]> { return getDocs('rx', eventId); }
  static async delete(x: Transmission): Promise<void> {
    const store = getTransmissionStore(await dbProm, x.txOrRx);
    let cursor = await store.index('event-hash').openCursor(IDBKeyRange.only([x.eventId, x.hash]));
    while (cursor) {
      await cursor.delete();
      cursor = await cursor.continue();
    }
  }

  async save(): Promise<void> {
    const store = getTransmissionStore(await dbProm, this.txOrRx);
    await store.put(dbConverter.toDB(this));
  }
}
export default Transmission;

// DB data converter
const dbConverter = {
  toDB(x: Transmission): ITransmissionDoc {
    let obj: ITransmissionDoc = {
      hash: x.hash,
      eventId: x.eventId,
      blocks: x.blocks,
      timestamp: x.timestamp,
    };
    if (x.entryCount) {
      obj.entryCount = x.entryCount;
    }
    if (x.hasAllChanges) {
      obj.hasAllChanges = true;
    }
    return obj;
  },
  fromDB(doc: ITransmissionDoc, txOrRx: 'tx' | 'rx') {
    const raceEventId = String(doc.eventId);
    const blocks: Amp['blocks'] = {};
    for (const key of Object.keys(doc.blocks) as Array<keyof Amp['blocks']>) {
      const b = doc.blocks[key as any];
      const keyword = b.keyword;
      const hash = b.hash;
      const data = b.data;
      switch(keyword) {
        case LTypes.DATA:
          blocks[key] = Block.MakeBlock({keyword, hash, data, blockNum: b.blockNum as number});
          break;
        case LTypes.CNTL:
          blocks[key] = Block.MakeBlock({keyword, hash, controlWord: b.controlWord as ControlWord});
          break;
        default:
          blocks[key] = Block.MakeBlock({keyword, hash, data});
      }
    }
    let x = new Transmission(raceEventId, {txOrRx, hash: doc.hash, blocks, entryCount: doc.entryCount});
    if (doc.hasAllChanges) { x.hasAllChanges = doc.hasAllChanges; }
    x.timestamp = new Date(doc.timestamp);
    return x;
  },
};
