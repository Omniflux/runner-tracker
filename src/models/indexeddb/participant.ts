import { IDBPDatabase } from 'idb';
import Base, { dbName as storeName, IParticipantDoc} from '../base/participant';
import { dbProm, RunnerTrackerDB } from './db';
import { Types } from '@hamstudy/mongodb-mobile-client';
import { getEntryStore } from './entry';

export function getParticipantStore(db: IDBPDatabase<RunnerTrackerDB>) {
  const store = db.transaction(storeName, 'readwrite').objectStore(storeName);
  return store;
}

export class Participant extends Base {
  static async getParticipant(eventId: string, bibNumber: number) {
    if (typeof bibNumber !== 'number') { throw new Error(`getParticipant expects bibNumber to be a number, but it was of type ${typeof bibNumber}`); }
    const store = (getParticipantStore(await dbProm)).index('event-bibNum');
    const doc = await store.get(IDBKeyRange.only([eventId, bibNumber]));
    return doc ? dbConverter.fromDB(doc) : null;
  }

  static async getParticipants(eventId: string, ids?: string[]) {
    const docs: Participant[] = [];
    const store = (getParticipantStore(await dbProm)).index('eventId');
    // let query = IDBKeyRange.only(raceEventId);
    // if (ids) {
    //   const [lower, upper] = [...ids].sort().splice(1, ids.length-2);
    //   query = (IDBKeyRange.bound(lower, upper));
    // }
    // const query = ids ? store.openCursor(IDB)
    let cursor = await store.openCursor(IDBKeyRange.only(eventId));
    while (cursor) {
      if (!ids || ids.indexOf(cursor.primaryKey) > -1) {
        docs.push(dbConverter.fromDB(cursor.value));
      }
      cursor = await cursor.continue();
    }
    return docs;
  }

  static async delete(participant: Participant) {
    let promises: Array<Promise<any>> = [];
    const entryStore = getEntryStore(await dbProm);
    let cursor = await entryStore.index('participantId').openKeyCursor(IDBKeyRange.only(participant.id));
    while (cursor) {
      promises.push(entryStore.delete(cursor.primaryKey));
      cursor = await cursor.continue();
    }
    const store = getParticipantStore(await dbProm);
    promises.push(store.delete(participant.id));
    await Promise.all(promises);
  }

  async save() {
    const store = getParticipantStore(await dbProm);
    await store.put(dbConverter.toDB(this));
  }
}
export default Participant;

// DB data converter
const dbConverter = {
  toDB(x: Participant) {
    x.id = x.id || (new Types.ObjectId()).toHexString();
    return x;
  },
  fromDB(doc: IParticipantDoc) {
    const raceEventId = String(doc.eventId);
    return new Participant(raceEventId, Object.assign({}, doc));
  },
};
