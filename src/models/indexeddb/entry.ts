import { IDBPDatabase } from 'idb';
import Base, { dbName as storeName, IEntryDoc} from '../base/entry';
import { dbProm, RunnerTrackerDB } from './db';
import { Types } from '@hamstudy/mongodb-mobile-client';
import { IDBPCursorWithValue, IndexNames, StoreNames } from 'idb';

export function getEntryStore(db: IDBPDatabase<RunnerTrackerDB>) {
  const store = db.transaction(storeName, 'readwrite').objectStore(storeName);
  return store;
}

export class Entry extends Base {
  static async getEntry(id: string): Promise<Entry | null>;
  static async getEntry(stationId: string, participantId: string): Promise<Entry | null>; // tslint:disable-line unified-signatures
  static async getEntry(id: string, participantId?: string): Promise<Entry | null> {
    const store = getEntryStore(await dbProm);
    let doc: IEntryDoc | undefined;
    if (!participantId) {
      doc = await store.get(id);
    } else {
      doc = await store.index('stationId-participantId').get(IDBKeyRange.only([id, participantId]));
    }
    return doc ? dbConverter.fromDB(doc) : null;
  }
  static async getEntrys({participantId, stationId, eventId, sinceDate}: Partial<{participantId: string; stationId: string; eventId: string, sinceDate: Date}>) {
    const docs: Entry[] = [];
    let cursor: IDBPCursorWithValue<RunnerTrackerDB, ["entrys"], "entrys", IndexNames<RunnerTrackerDB, "entrys">, "readwrite"> | null = null;
    if (participantId) {
      cursor = await (getEntryStore(await dbProm)).index('participantId').openCursor(IDBKeyRange.only(participantId), 'prev');
    } else if (stationId) {
      if (sinceDate) {
        cursor = await (getEntryStore(await dbProm)).index('stationId-modified').openCursor(IDBKeyRange.bound([stationId, sinceDate], [stationId, new Date()]), 'prev');
      } else {
        cursor = await (getEntryStore(await dbProm)).index('stationId').openCursor(IDBKeyRange.only(stationId), 'prev');
      }
    } else if (eventId) {
      cursor = await (getEntryStore(await dbProm)).index('eventId').openCursor(IDBKeyRange.only(eventId), 'prev');
    }
    while (cursor) {
      docs.push(dbConverter.fromDB(cursor.value));
      cursor = await cursor.continue();
    }
    return docs;
  }

  static async delete(entry: Entry) {
    const store = getEntryStore(await dbProm);
    await store.delete(entry.id);
  }

  async save() {
    const store = getEntryStore(await dbProm);
    await store.put(dbConverter.toDB(this));
  }
}
export default Entry;

// DB data converter
const dbConverter = {
  toDB(x: Entry) {
    x.id = x.id || (new Types.ObjectId()).toHexString();
    x.modified = new Date();
    return x;
  },
  fromDB(doc: IEntryDoc) {
    const raceEventId = String(doc.eventId);
    const participantId = String(doc.participantId);
    const stationId = String(doc.stationId);
    return new Entry(raceEventId, participantId, stationId, Object.assign({}, doc));
  },
};
