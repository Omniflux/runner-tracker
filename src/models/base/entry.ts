export const dbName = <const>'entrys';

// tslint:disable max-classes-per-file
abstract class EntryDoc {
  id!: string;
  eventId!: string;
  participantId!: string;
  stationId!: string;

  modified!: Date;
  timeIn: Date | null = null;
  timeOut: Date | null = null;
}
export type IEntryDoc = EntryDoc;
export class Entry extends EntryDoc {
  static async getEntry(id: string): Promise<Entry | null>;
  static async getEntry(stationId: string, participantId: string): Promise<Entry | null>; // tslint:disable-line unified-signatures
  static async getEntry(id: string, participantId?: string): Promise<Entry | null> { throw new Error("Can't use base model"); }
  static async getEntrys({participantId, stationId, eventId, sinceDate}: Partial<{participantId: string; stationId: string; eventId: string; sinceDate: Date}>): Promise<Entry[]> { throw new Error("Can't use base model"); }
  static async delete(entry: Entry): Promise<void> { return; }

  constructor(
    eventId: string,
    participantId: string,
    stationId: string,
    obj: Partial<Entry> = {},
  ) {
    super();
    Object.assign(this, obj, {eventId, participantId, stationId});
    if (!this.id && !this.modified) { this.modified = new Date(); }
  }

  async save(): Promise<void> { return; }
}
export default Entry;
