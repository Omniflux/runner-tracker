export const dbName = <const>'stations';

// tslint:disable max-classes-per-file
abstract class StationDoc {
  id!: string;
  eventId!: string;
  name?: string;
  stationNumber!: number;
  stationNumberDisplayed!: string;
  distance?: number;
}
export type IStationDoc = StationDoc;
export class Station extends StationDoc {
  // This is used to verify that the station id we have belongs to this event, if not it'll be null
  static async getStation(obj: {eventId: string; stationId?: string}): Promise<Station | null>; // tslint:disable-line unified-signatures
  static async getStation(obj: {eventId: string; stationNumber: number}): Promise<Station | null>; // tslint:disable-line unified-signatures
  static async getStation(obj: Partial<{eventId: string; stationId: string; stationNumber: number}>): Promise<Station | null> { throw new Error("Can't use base model"); }
  static async getStations(eventId: string): Promise<Station[]> { throw new Error("Can't use base model"); }
  static async delete(station: Station): Promise<void> { throw new Error("Can't use base model"); }

  constructor(
    eventId: string,
    obj: Partial<Station> & Pick<Station, 'stationNumber'>,
  ) {
    super();
    Object.assign(this, obj, {eventId});
  }
  async save(): Promise<void> { throw new Error("Can't use base model"); }
}
export default Station;
