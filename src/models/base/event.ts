export const dbName = <const>'events';

// tslint:disable max-classes-per-file
abstract class EventDoc {
  id!: string;
  name!: string;
  startDate?: Date;
  endDate?: Date;
}
export type IEventDoc = EventDoc;
export class Event extends EventDoc {
  static async getEvent(id: string): Promise<Event | null> { return null; }
  static async getEvents(): Promise<Event[]> { return []; }
  static async delete(event: Event): Promise<void> { return; }

  constructor(
    obj: Partial<Event> & Pick<Event, 'name'>,
  ) {
    super();
    Object.assign(this, obj);
  }
  async save(): Promise<void> { return; }
}
export default Event;
