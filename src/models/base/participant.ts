import {Station} from '@/models/station';
import {Entry} from '@/models/entry';

export const dbName = <const>'participants';

// tslint:disable max-classes-per-file
abstract class ParticipantDoc {
  id!: string;
  age?: string;
  bibNumber!: number;
  firstName?: string;
  lastName?: string;
  modified?: Date;
  note?: string;
  home?: string;
  eventId!: string;
  sex?: 'M' | 'F';
  team?: string;
  dnfReason = '';
  dnfStation: number | null = null;
}
export type IParticipantDoc = ParticipantDoc;
export class Participant extends ParticipantDoc {
  static async getParticipant(raceEventId: string, bibNumber: number): Promise<Participant | null> { throw new Error("Can't use base model"); }
  static async getParticipants(raceEventId: string, ids?: string[]): Promise<Participant[]> { throw new Error("Can't use base model"); }
  static async delete(participant: Participant) { throw new Error("Can't use base model"); }

  constructor(
    raceEventId: string,
    obj: Partial<Participant> & Pick<Participant, 'bibNumber'>,
  ) {
    super();
    Object.assign(this, obj, {eventId: raceEventId});

    if (isNaN(Number(this.bibNumber))) { throw new Error('Tried to create a runner, but runner obj is missing valid bibNumber'); }
    return this;
  }
  async save(): Promise<void> { return; }

  drop({reason, station}: {reason: string, station: Station}) {
    this.dnfStation = station.stationNumber;
    this.dnfReason = reason;
  }
  undrop() {
    this.dnfStation = null;
    this.dnfReason = '';
  }
}
export default Participant;
